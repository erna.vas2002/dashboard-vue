import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import SignInPage from '@/views/sign-in/SignInPage.vue'
import ProfilePage from '@/views/ProfilePage.vue'
import CreateEmployeePage from '@/views/CreateEmployeePage.vue'
import EditEmployeePage from '@/views/EditEmployeePage.vue'
import VehiclesPage from '@/views/VehiclesPage.vue'
import CreateVehiclesPage from '@/views/CreateVehiclesPage.vue'
import EditVehiclesPage from '@/views/EditVehiclesPage.vue'
import CompaniesPage from '@/views/CompaniesPage.vue'
import CreateCompaniesPage from '@/views/CreateCompaniesPage.vue'
import EditCompaniesPage from '@/views/EditCompaniesPage.vue'
import SettingPage from '@/views/sign-in/SignInPage.vue'

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		meta: { layout: 'login', title: 'Demo Test' },
		component: SignInPage,
	},
	{
		path: '/profile',
		meta: { layout: 'main', title: 'Profile' },
		component: ProfilePage,
	},
	{
		path: '/staff/:id',
		meta: { layout: 'main' },
		component: EditEmployeePage,
	},
	{
		path: '/create-employee',
		meta: { layout: 'main', title: 'New Employee' },
		component: CreateEmployeePage,
	},
	{
		path: '/vehicles',
		meta: { layout: 'main', title: 'Vehicles' },
		component: VehiclesPage,
	},
	{
		path: '/vehicles/:id',
		meta: { layout: 'main' },
		component: EditVehiclesPage,
	},
	{
		path: '/create-vehicles',
		meta: { layout: 'main', title: 'New vehicles' },
		component: CreateVehiclesPage,
	},
	{
		path: '/companies',
		meta: { layout: 'main', title: 'Companies' },
		component: CompaniesPage,
	},
	{
		path: '/create-companies',
		meta: { layout: 'main', title: 'New companies' },
		component: CreateCompaniesPage,
	},
	{
		path: '/companies/:id',
		meta: { layout: 'main' },
		component: EditCompaniesPage,
	},
	{
		path: '/setting',
		meta: { layout: 'main', title: 'Setting' },
		component: SettingPage,
	},
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
})

export default router
