export const vehicles = {
	state: {
		cars: [
			{
				vin: 'WDB 1400321A333419',
				make: 'Mercedes-Benz 1',
				model: 'C-Class',
				id: 1703089127896,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333418',
				make: 'Mercedes-Benz 2',
				model: 'C-Class',
				id: 1703089127816,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333417',
				make: 'Mercedes-Benz 3',
				model: 'C-Class',
				id: 1703089127895,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333488',
				make: 'Mercedes-Benz 4',
				model: 'C-Class',
				id: 1703089127896,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333487',
				make: 'Mercedes-Benz 5',
				model: 'C-Class',
				id: 1703089127816,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333481',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127895,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333416',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127815,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333415',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127894,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333414',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127813,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333413',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127892,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333412',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127811,
				img: 'car-table.png',
				number: 0,
			},
			{
				vin: 'WDB 1400321A333411',
				make: 'Mercedes-Benz',
				model: 'C-Class',
				id: 1703089127890,
				img: 'car-table.png',
				number: 0,
			},
		],
		imagesCar: [
			{
				position: 1,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 2,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 3,
				carImg: 'car-table.png',
				defaultImage: 'car-table.png',
			},
			{
				position: 4,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 5,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 6,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 7,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 8,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 9,
				carImg: 'car-table.png',
				defaultImage: 'car-table.png',
			},
			{
				position: 10,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 11,
				carImg: 'car-table.png',
				defaultImage: '',
			},
			{
				position: 12,
				carImg: 'car-table.png',
				defaultImage: '',
			},
		],
		totalPage: 1,
		currentPage: 1,
		limitCountCars: 9,
	},
	getters: {
		getAllCars: s => s.cars,

		getImages: s => s.imagesCar,

		getSearchByMake: (s, getters) => make =>
			getters.getAllCars
				.filter(car => car.make.toLowerCase().includes(make.toLowerCase()))
				.slice(
					(s.currentPage - 1) * s.limitCountCars,
					(s.currentPage - 1) * s.limitCountCars + s.limitCountCars
				),

		getCountAllPagesWithCars: (s, getters) =>
			Math.ceil(getters.getAllCars.length / s.limitCountCars),

		carPerPage: (s, getters) =>
			s.limitCountCars * s.currentPage >= getters.getAllCars.length
				? getters.getAllCars.length
				: s.limitCountCars * s.currentPage,

		getCurrentPageCars: s => {
			return pageNumber => (s.currentPage = pageNumber)
		},

		getImageFilter: (s, getters) => checkBoxValue =>
			getters.getImages.filter(item => console.log(item)),
	},

	mutations: {
		createCar(state, car) {
			state.cars = [car, ...state.cars]
		},

		setRemoveCar: (s, id) =>
			s.cars.splice(
				s.cars.filter(item => item.id !== id),
				1
			),
	},
}
