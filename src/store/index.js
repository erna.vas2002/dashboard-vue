import { createStore } from 'vuex'
import { profile } from "@/store/profile";
import { vehicles } from "@/store/vehicles";

export default createStore({
  modules: {
    profile,
    vehicles,
  }
})
