export const profile = {
	state: {
		employees: [
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127826,
				name: 'gg',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '2',
				email: 'supra20143@gmail.com',
				id: 1703089127825,
				name: 'kk',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127824,
				name: 'jk',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127823,
				name: 'df',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127822,
				name: 'tghj',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127821,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127820,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127827,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127828,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127829,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127830,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 17030891278331,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127832,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 17030891278366,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
			{
				credit: '0',
				email: 'supra20143@gmail.com',
				id: 1703089127865,
				name: '123',
				phone: 'asda',
				tokens: '123',
			},
		],

		admin: {
			id: 17030800127826,
			name: 'John Doe',
			email: 'name@domian.com',
			password: '12345678',
			phone: '+497074559490',
			credit: 79,
			tokens: 12,
		},

		limitCountEmployees: 12,
		currentPage: 1,
	},

	getters: {
		getAllEmployees: s => s.employees,

		getAdminDetails: s => s.admin,

		getEmployeeById(state) {
			return id => state.employees.find(item => item.id === Number(id)) || {}
		},

		getNameSearchAndLimitEmployees: (s, getters) => name =>
			getters.getAllEmployees
				.filter(item => item.name.toLowerCase().includes(name.toLowerCase()))
				.slice(
					(s.currentPage - 1) * s.limitCountEmployees,
					(s.currentPage - 1) * s.limitCountEmployees + s.limitCountEmployees
				),

		getCountAllPages: (s, getters) =>
			Math.ceil(getters.getAllEmployees.length / s.limitCountEmployees),

		getCurrentPage: s => {
			return pageNumber => (s.currentPage = pageNumber)
		},

		employeePerPage: (s, getters) =>
			s.limitCountEmployees * s.currentPage >= getters.getAllEmployees.length
				? getters.getAllEmployees.length
				: s.limitCountEmployees * s.currentPage,
	},

	mutations: {
		createEmployees(state, employee) {
			state.employees = [employee, ...state.employees]
		},
	},

	actions: {},
}
